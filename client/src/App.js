import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/layout/Header";
import Todos from "./components/Todos";
import AddTodo from "./components/AddTodo";
import About from "./components/pages/About";
// import { v4 as uuidv4 } from "uuid";
import axios from "axios";

import "./App.css";

class App extends Component {
  state = {
    todos: [],
  };

  componentDidMount() {
    axios
      .get("http://localhost:5000/api/todos")
      .then((res) => this.setState({ todos: res.data }));
  }

  //Toggle complete
  markComplete = (_id) => {
    axios.put("http://localhost:5000/api/todos/:id").then(
      this.setState({
        todos: this.state.todos.map((todo) => {
          if (todo._id === _id) {
            todo.completed = !todo.completed;
          }
          return todo;
        }),
      })
    );
  };

  //Delete Todo
  delTodo = (_id) => {
    axios.delete(`http://localhost:5000/api/todos/${_id}`).then((res) =>
      this.setState({
        todos: [...this.state.todos.filter((todo) => todo._id !== _id)],
      })
    );
  };

  //Add Todo
  addTodo = (title) => {
    // const newTodo = {
    //   id: uuidv4(),
    //   title,
    //   completed: false,
    // };
    axios
      .post("http://localhost:5000/api/todos", {
        userId: 1,
        title,
      })
      .then((res) => this.setState({ todos: [...this.state.todos, res.data] }));
  };

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route
              exact
              path="/"
              render={(props) => (
                <React.Fragment>
                  <AddTodo addTodo={this.addTodo} />
                  <Todos
                    todos={this.state.todos}
                    markComplete={this.markComplete}
                    delTodo={this.delTodo}
                  />
                </React.Fragment>
              )}
            />
            <Route path="/about" component={About} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
