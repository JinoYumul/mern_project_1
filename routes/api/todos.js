const express = require("express");
const router = express.Router();

//Model
const Todo = require("../../models/Todo");

//Create
router.post("/", (req, res) => {
  const newTodo = new Todo({
    userId: req.body.userId,
    title: req.body.title,
  });

  newTodo.save().then((todo) => res.json(todo));
});

//Get all
router.get("/", (req, res) => {
  Todo.find().then((todos) => res.json(todos));
});

//Get specific
router.get("/:id", (req, res) => {
  Todo.findById(req.params.id)
    .then((todo) => res.json(todo))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

//Update
// router.put("/update/:id", (req, res) => {
//   if (req.body.title == null) {
//     res.status(400).json({ err: "Please enter a title" });
//   } else {
//     Todo.findByIdAndUpdate(
//       req.params.id,
//       {
//         title: req.body.title,
//       },
//       { new: true }
//     )
//       .then((todo) => res.json(todo))
//       .catch((err) => res.status(404).json({ err: "Not found" }));
//   }
// });

// router.put("/update/:id", (req, res) => {
//   Todo.findByIdAndUpdate(
//     req.params.id,
//     {
//       completed: !completed,
//     },
//     { new: true }
//   ).then((todo) => res.json(todo));
// });

//Delete
router.delete("/:id", (req, res) => {
  Todo.findById(req.params.id)
    .then((todo) => todo.remove().then(() => res.json({ err: "Todo deleted" })))
    .catch((err) => res.status(404).json({ err: "Not found" }));
});

module.exports = router;
